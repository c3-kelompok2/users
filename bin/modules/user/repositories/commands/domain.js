
const Query = require('../queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const jwtAuth = require('../../../../auth/jwt_auth_helper');
const commonUtil = require('../../../../helpers/utils/common');
const logger = require('../../../../helpers/utils/logger');
const model = require('./command_model')
const command = require('./command');
const validate = require('validate.js');
const { NotFoundError, UnauthorizedError, ConflictError } = require('../../../../helpers/error');

class User {

  async addUser(payload){
    const data = [payload];
    let view = model.users();
    view = data.reduce((accumulator, value) => {
        if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
        if(!validate.isEmpty(value.username)){accumulator.username = value.username;}
        if(!validate.isEmpty(value.password)){accumulator.password = value.password;}
        if(!validate.isEmpty(value.email)){accumulator.email = value.email;}
        if(!validate.isEmpty(value.notelp)){accumulator.notelp = value.notelp;}
        if(!validate.isEmpty(value.name)){accumulator.name = value.name;}          
        return accumulator;
    }, view);
    const document = view;
    const result = await command.insertOne(document);
    return result;
}

async updateUser(payload){
  const data = [payload];
  let view = model.users();
  view = data.reduce((accumulator, value) => {
      if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
      if(!validate.isEmpty(value.username)){accumulator.username = value.username;}
      if(!validate.isEmpty(value.password)){accumulator.password = value.password;}
      if(!validate.isEmpty(value.email)){accumulator.email = value.email;}
      if(!validate.isEmpty(value.notelp)){accumulator.notelp = value.notelp;}
      if(!validate.isEmpty(value.name)){accumulator.name = value.name;}          
      return accumulator;
  }, view);
  const document = view;
  const result = await command.updateOne(document);
  return result;
}

async deleteUser(params){
  const result = await command.deleteOne(params);
  return result;
}

}

module.exports = User;
